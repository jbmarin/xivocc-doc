************
Introduction
************

XiVO-CC provides enhancements of the XiVO PBX contact center functionalities.

It gives especially access to outsourced statistics, real-time supervision screens, third-party CTI
integration and recording functionalities.

Xuc is an application suite developed by Avencall_ Group, based on several free existing components
including XiVO_, and our own developments to provide communication services api and application to
businesses.
XiVO-CC is build on Play_ using intensively Akka_ and written in Scala_

.. _Avencall: http://www.avencall.com/
.. _XiVO: http://documentation.xivo.io/
.. _Play: http://www.playframework.com/
.. _Akka: http://akka.io/
.. _Scala: http://www.scala-lang.org/
.. _Gitlab: https://gitlab.com/groups/xivoxc

XiVO is `free software`_. Most of its distinctive components, and XiVO-CC as a whole, are distributed
under the *LGPLv3 license*.

.. _free software: http://www.gnu.org/philosophy/free-sw.html

XiVO-CC is providing

* Javascript API
* Web services
* Sample application
* Agent application
* Unified communication application pratix
* Contact center supervision
* Contact center statistics
* Reporting

The proposed applications are available in English and French. The list of preferred langs sent by
the browser is analyzed and the first known lang is used, so if the browser requests it, en and fr
the page will be server in en. The fallback language is French. Contributions are welcome, start
with opening an issue on Gitlab_ project page.