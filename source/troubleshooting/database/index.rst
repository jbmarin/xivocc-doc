*******************
Database (postgres)
*******************

Database container keeps restarting
===================================

After updating to the latest version of the components (``dcomp pull`` and ``dcomp up -d``), you may see the database container (``xivocc_pgxivocc_1``) restarting continuously.
This may be caused by invalid rights on the folder containing the database data files.

To circumvent the problem, you first need to identify the containing folder. Run the command ``docker inspect xivocc_pgxivocc_1`` and look for the mounts section::
  
    "Mounts": [
           {
               "Source": "/etc/localtime",
               "Destination": "/etc/localtime",
               "Mode": "",
               "RW": false,
               "Propagation": "rprivate"
           },
           {
               "Source": "/etc/timezone",
               "Destination": "/etc/timezone",
               "Mode": "",
               "RW": false,
               "Propagation": "rprivate"
           },
           {
               "Name": "246bde6eb960ba115c35b673930984219dc01ad7ec5d6d11b690202a5cc6b106",
               "Source": "/var/lib/docker/volumes/246bde6eb960ba115c35b673930984219dc01ad7ec5d6d11b690202a5cc6b106/_data",
               "Destination": "/var/lib/postgresql/data",
               "Driver": "local",
               "Mode": "rw",
               "RW": true,
               "Propagation": "rprivate"
	   }


Here, the local folder is ``/var/lib/docker/volumes/246bde6eb960ba115c35b673930984219dc01ad7ec5d6d11b690202a5cc6b106/_data``.

Then change the folder right by running the command: ``chmod 700 /var/lib/docker/volumes/246bde6eb960ba115c35b673930984219dc01ad7ec5d6d11b690202a5cc6b106/_data/`` and then restart the container ``dcomp restart pgxivocc``.
