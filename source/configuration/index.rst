*************
Configuration
*************

You can find in this section how to configure your XiVO-CC  system.

.. toctree::
   :maxdepth: 2

   acd_call
   totem_panel
   webrtc/webrtc
   xuc
   xucmgt	      
   phone_integration
   pack_reporting/index
