.. _admin:

**************
Administration
**************

Log Files
---------

The log of each components can be found in the `/var/log/xivocc` directory.
Currently (it may change) the structure looks like this :

.. code-block:: sh
    
    /var/log/xivocc :
    ├── purge-reporting-database.log
    ├── specific-stats.log
    ├── xivo-db-replication.log
    ├── xivo-full-stats.log
        ├── recording-server
    │   ├── dowloads.log
    │   ├── downloads.log
    │   └── recording-server.log
    ├── xuc
    │   └── xuc.log
    └── xucmgt
        └── xucmgt.log


Changing Log Level
------------------

Log level can be changed for the xuc server by editing /opt/docker/conf/logger.xml file inside the container

Backup
------

You may backup your statistic database by using a similar command as below

.. code-block:: ini

    docker  run --rm --link demo_pgxivocc_1:db -v $(pwd):/backup -e PGPASSWORD=xivocc postgres pg_dump -h db -U postgres --format=c -f /backup/xivo_stats_dump xivo_stats


Restore
-------

You may restore a backup using a similar command (to be adapted)

.. code-block:: ini

    docker run --rm -it --link pgxivoccdemo_pgxivocc_1:db -v $(pwd):/backup postgres pg_restore -h db -c -U postgres -d xivo_stats /backup/xivo_stats_dump